import * as React from 'react';
import * as Facebook from 'expo-facebook';
import { Text, View, StyleSheet, TouchableOpacity, Image, TextInput, Button } from 'react-native';
import { Entypo, AntDesign, MaterialCommunityIcons } from 'react-native-vector-icons';
import color from '../../constants/color'

export default class LoginScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    }
  }

  NavigateSignUpButton = () =>{
    this.props.navigation.navigate('Signup')
  }
  

  LoginWithFacebook = async() => {

    const {
      type,
      token,
      
    } = await 
    // Facebook.initializeAsync('398303244218630', 'v9.0');
    Facebook.logInWithReadPermissionsAsync('410020393487894', {
      permissions: ['public_profile'],
    });
    
    if (type === 'success') {
      this.props.navigation.navigate('Main')
    } else {
      this.props.navigation.navigate('Main')
    }
    }
  render() {
    return (
      <View style={styles.container}>
        <TextInput style={styles.TextInput} placeholder="Phone number or Email" placeholderTextColor='gray' />
        <TextInput style={styles.TextInput} placeholder="Password" secureTextEntry={true} placeholderTextColor='gray' />
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Main')} style={styles.LoginButton}>
          <Text style={styles.LoginText}>Login</Text>
        </TouchableOpacity>
        <View style={styles.textOr}>
          <View style={{ marginRight: 10, borderTopWidth: 0.5, borderBottomWidth: 0.5, borderColor: 'grey', width: 105, height: 0 }} />
          <View>
            <Text style={{ color: 'grey' }}>OR</Text>
          </View>
          <View style={{ marginLeft: 10, borderTopWidth: 0.5, borderBottomWidth: 0.5, borderColor: 'grey', width: 105, height: 0 }} />
        </View>


        <View style={styles.buttonformat}>
          <TouchableOpacity>
            <Entypo name="facebook-with-circle" onPress={this.LoginWithFacebook} size={30} color='blue'></Entypo>
          </TouchableOpacity>
          <TouchableOpacity>
            <MaterialCommunityIcons name="gmail" onPress={this.LoginWithGoogle} size={30} color='red' />
          </TouchableOpacity>
        </View>
        <Button style={styles.ForgotPwText} onPress={this.ForgotPassword} title='Fogot Password?' color='grey' size={14} fontWeight='bold' />

        <View style={styles.Register}>
          <Text style={{ fontSize: 17 }}>Don't have an account?</Text>
          <Button style={styles.ForgotPwText} onPress={this.NavigateSignUpButton} color={color.darkPink} title='SIGN UP NOW!?' fontWeight='bold' />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 8,
  },
  Logo: {
    width: 200,
    height: 50
  },
  TextInput: {
    padding: 10,
    marginTop: 20,
    height: 40,
    width: 250,
    borderWidth: 1,
    borderColor: color.darkPink,
    borderRadius: 5
  },
  LoginButton: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: color.darkPink,
    width: 250,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5
  },
  LoginText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonformat: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between', width: 250,
  },
  ForgotPwText: {
    marginTop: 20,
    marginBottom: 20,
    textAlign: 'center',
    fontSize: 15,
    textDecorationLine: 'underline',
  },
  Register: {
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center"
  },
  textOr: {
    flexDirection: "row",
    justifyContent: 'space-between',
    alignItems: 'center'
  }
});

// import React, { useState } from 'react';
// import { StyleSheet, Text, View, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
// import * as Facebook from 'expo-facebook';

// console.disableYellowBox = true;

// export default function App() {

//   const [isLoggedin, setLoggedinStatus] = useState(false);
//   const [userData, setUserData] = useState(null);
//   const [isImageLoading, setImageLoadStatus] = useState(false);

//   const facebookLogIn = async () => {
//     try {
//       const {
//         type,
//         token,
//         expires,
//         permissions,
//         declinedPermissions,
//       } = await 
//       // Facebook.initializeAsync('410020393487894')
//       Facebook.logInWithReadPermissionsAsync('410020393487894', {
//         permissions: ['public_profile'],
//       });
//       if (type === 'success') {
//         // Get the user's name using Facebook's Graph API
//         fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.height(500)`)
//           .then(response => response.json())
//           .then(data => {
//             setLoggedinStatus(true);
//             setUserData(data);
//           })
//           .catch(e => console.log(e))
//       } else {
//         // type === 'cancel'
//       }
//     } catch ({ message }) {
//       alert(`Facebook Login Error: ${message}`);
//     }
//   }

//   const logout = () => {
//     setLoggedinStatus(false);
//     setUserData(null);
//     setImageLoadStatus(false);
//   }

//   return (
//     isLoggedin ?
//       userData ?
//         <View style={styles.container}>
//           <Image
//             style={{ width: 200, height: 200, borderRadius: 50 }}
//             source={{ uri: userData.picture.data.url }}
//             onLoadEnd={() => setImageLoadStatus(true)} />
//           <ActivityIndicator size="large" color="#0000ff" animating={!isImageLoading} style={{ position: "absolute" }} />
//           <Text style={{ fontSize: 22, marginVertical: 10 }}>Hi {userData.name}!</Text>
//           <TouchableOpacity style={styles.logoutBtn} onPress={() => logout()}>
//             <Text style={{ color: "#fff" }}>Logout</Text>
//           </TouchableOpacity>
//         </View> :
//         null
//       :
//       <View style={styles.container}>
//         <Image
//           style={{ width: 200, height: 200, borderRadius: 50, marginVertical: 20 }}
//          />
//         <TouchableOpacity style={styles.loginBtn} onPress={() => facebookLogIn()}>
//           <Text style={{ color: "#fff" }}>Login with Facebook</Text>
//         </TouchableOpacity>
//       </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#e9ebee',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   loginBtn: {
//     backgroundColor: '#4267b2',
//     paddingVertical: 10,
//     paddingHorizontal: 20,
//     borderRadius: 20
//   },
//   logoutBtn: {
//     backgroundColor: 'grey',
//     paddingVertical: 10,
//     paddingHorizontal: 20,
//     borderRadius: 20,
//     position: "absolute",
//     bottom: 0
//   },
// });